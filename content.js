// --------------------------------------- H1 ---------------------------------------

var HB_SC_CLASSES = [
    "sc-w",
    "sc-b",
    "sc-d",
    "sc-m",
    "sc-ml",
    "sc-ac sc-a",
    "sc-acl sc-a",
    "sc-acd sc-a",
    "sc-cd sc-a",
    "sc-cm sc-a",
    "sc-cl sc-al",
    "sc-cs sc-al",
];

var AC_PAIRS = [
    {"ac-name": "ac-ffcdd2",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-f8bbd0",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-e1bee7",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-c5cae9",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-bbdefb",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-b2ebf2",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-al",
        "sc-acd": "sc-al"
        }},
    {"ac-name": "ac-b2dfdb",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-al",
        "sc-acd": "sc-al"
        }},
    {"ac-name": "ac-c8e6c9",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-al",
        "sc-acd": "sc-al"
        }},
    {"ac-name": "ac-c5e1a5",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-al",
        "sc-acd": "sc-al"
        }},
    {"ac-name": "ac-e6ee9c",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-al",
        "sc-acd": "sc-al"
        }},
    {"ac-name": "ac-fff176",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-al",
        "sc-acd": "sc-al"
        }},
    {"ac-name": "ac-ffe0b2",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-al",
        "sc-acd": "sc-al"
        }},
    {"ac-name": "ac-ffccbc",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-al",
        "sc-acd": "sc-al"
        }},
    {"ac-name": "ac-d7ccc8",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-al",
        "sc-acd": "sc-al"
        }},
    {"ac-name": "ac-e0e0e0",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-al",
        "sc-acd": "sc-al"
        }},
    {"ac-name": "ac-cfd8dc",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-al",
        "sc-acd": "sc-al"
        }},
    {"ac-name": "ac-ef9e9e",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-f48fb1",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-ce93d8",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-9fa8da",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-90caf9",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-80deea",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-al",
        "sc-acd": "sc-al"
        }},
    {"ac-name": "ac-80cbc4",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-a5d6a7",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-aed581",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-dce775",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-al",
        "sc-acd": "sc-al"
        }},
    {"ac-name": "ac-ffeb3b",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-al",
        "sc-acd": "sc-al"
        }},
    {"ac-name": "ac-ffcc80",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-al",
        "sc-acd": "sc-al"
        }},
    {"ac-name": "ac-ffab91",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-bcaaa4",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-bdbdbd",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-b0bec5",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-f25151",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-ed5891",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-ba68c8",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-7986cb",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-64b5f6",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-26c6da",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-35bca8",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-81c784",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-9ccc65",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-cddc39",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-fdd835",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-al",
        "sc-acd": "sc-al"
        }},
    {"ac-name": "ac-ffb74d",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-ff8a65",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-a1887f",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-9e9e9e",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-90a4ae",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-ed2e29",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-ea2d75",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-ab47bc",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-5c6bc0",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-2196f3",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-00acc1",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-009688",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-4caf50",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-8bc34a",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-c0ca33",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-fbc02d",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-ffa726",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-ff7043",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-8d6e63",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-757575",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-607d8b",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-d82323",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-c2185b",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-7b2ba5",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-3a4ca5",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-1976d2",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-00838f",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-00796b",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-388e3c",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-689f38",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-afb42b",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-f9a825",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-ea7b28",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-e84b21",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-6d4c41",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-616161",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-455a64",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-ad0c0c",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-880e4f",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-502193",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-1a237e",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-0d47a1",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-006064",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-004d40",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-1b5e20",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-457c2c",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-91822c",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-f57f17",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-e25827",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-bf360c",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-4f332f",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-3a3a3a",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-263238",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-7f0d0d",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-59073a",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-331168",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-122559",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-07346b",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-004747",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-00352b",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-123f13",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-275115",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-665a14",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-d65c09",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-b52f00",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-7c0f00",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-3f2726",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-141414",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-141b1e",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-none",
     "ac-pairings": {
        "sc-ac": "sc-m",
        "sc-acl": "sc-m",
        "sc-acd": "sc-m"
        }},
    {"ac-name": "ac-chestnut",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-viola",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-mint",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-cyan",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-blue",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-black",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-brick",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-danube",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-dust",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-gray",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-green",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-olive",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-damask",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-mandy",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-salmon",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-violet",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-wine",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-beige",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-dodger",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-forest",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-ggrass",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-mred",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-amber",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-base",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-brown",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-dolly",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-al",
        "sc-acd": "sc-al"
        }},
    {"ac-name": "ac-orange",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-pink",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-teal",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-lblue",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-body",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-coral",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-lemon",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-al",
        "sc-acd": "sc-al"
        }},
    {"ac-name": "ac-must",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-al",
        "sc-acd": "sc-al"
        }},
    {"ac-name": "ac-purple",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-trose",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-sandal",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-borange",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-lbrown",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-dblue",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-gold",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-gsand",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-rose",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-sky",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-tan",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-tgold",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-twine",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-yellow",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-can",
     "ac-pairings": {
        "sc-ac": "sc-al",
        "sc-acl": "sc-al",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-punch",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }},
    {"ac-name": "ac-wnd",
     "ac-pairings": {
        "sc-ac": "sc-a",
        "sc-acl": "sc-a",
        "sc-acd": "sc-a"
        }}
    ];


var CS_PAIRS = [
    {"cs-name": "cs-autumn",
     "cs-pairings": {
        "sc-cd": "sc-a",
        "sc-cm": "sc-a",
        "sc-cl": "sc-a",
        "sc-cs": "sc-al",
        }},
    {"cs-name": "cs-cherry",
     "cs-pairings": {
        "sc-cd": "sc-a",
        "sc-cm": "sc-a",
        "sc-cl": "sc-a",
        "sc-cs": "sc-al",
        }},
    {"cs-name": "cs-dark",
     "cs-pairings": {
        "sc-cd": "sc-a",
        "sc-cm": "sc-a",
        "sc-cl": "sc-al",
        "sc-cs": "sc-al",
        }},
    {"cs-name": "cs-dblue",
     "cs-pairings": {
        "sc-cd": "sc-a",
        "sc-cm": "sc-a",
        "sc-cl": "sc-a",
        "sc-cs": "sc-al",
        }},
    {"cs-name": "cs-december",
     "cs-pairings": {
        "sc-cd": "sc-a",
        "sc-cm": "sc-a",
        "sc-cl": "sc-a",
        "sc-cs": "sc-al",
        }},
    {"cs-name": "cs-deep",
     "cs-pairings": {
        "sc-cd": "sc-a",
        "sc-cm": "sc-a",
        "sc-cl": "sc-a",
        "sc-cs": "sc-al",
        }},
    {"cs-name": "cs-dim",
     "cs-pairings": {
        "sc-cd": "sc-a",
        "sc-cm": "sc-a",
        "sc-cl": "sc-al",
        "sc-cs": "sc-al",
        }},
    {"cs-name": "cs-fiord",
     "cs-pairings": {
        "sc-cd": "sc-a",
        "sc-cm": "sc-a",
        "sc-cl": "sc-a",
        "sc-cs": "sc-al",
        }},
    {"cs-name": "cs-gray",
     "cs-pairings": {
        "sc-cd": "sc-a",
        "sc-cm": "sc-a",
        "sc-cl": "sc-al",
        "sc-cs": "sc-al",
        }},
    {"cs-name": "cs-gray-3",
     "cs-pairings": {
        "sc-cd": "sc-a",
        "sc-cm": "sc-a",
        "sc-cl": "sc-a",
        "sc-cs": "sc-al",
        }},
    {"cs-name": "cs-green",
     "cs-pairings": {
        "sc-cd": "sc-a",
        "sc-cm": "sc-a",
        "sc-cl": "sc-a",
        "sc-cs": "sc-al",
        }},
    {"cs-name": "cs-lblue",
     "cs-pairings": {
        "sc-cd": "sc-a",
        "sc-cm": "sc-a",
        "sc-cl": "sc-a",
        "sc-cs": "sc-al",
        }},
    {"cs-name": "cs-orange",
     "cs-pairings": {
        "sc-cd": "sc-a",
        "sc-cm": "sc-a",
        "sc-cl": "sc-a",
        "sc-cs": "sc-al",
        }},
    {"cs-name": "cs-sky",
     "cs-pairings": {
        "sc-cd": "sc-a",
        "sc-cm": "sc-a",
        "sc-cl": "sc-a",
        "sc-cs": "sc-al",
        }},
    {"cs-name": "cs-west",
     "cs-pairings": {
        "sc-cd": "sc-a",
        "sc-cm": "sc-a",
        "sc-cl": "sc-a",
        "sc-cs": "sc-al",
        }},
];
// ---------------------------------------------------------------------------------------------------------------------

var applyStyles = function(styleData){

    setTimeout(function(){

        var pageClasses =  $(".wnd-page").attr("class"),
            pairedColor = "";

        if(styleData.HB_SC === "sc-ac" || styleData.HB_SC === "sc-acl" || styleData.HB_SC === "sc-acd") {

            AC_PAIRS.forEach(function(AC){
                // if(CS["cs-name"] === currentCS){
                if(pageClasses.indexOf(AC["ac-name"]) !== -1){
                    pairedColor = " " + AC["ac-pairings"][styleData.HB_SC];
                    console.log(AC["ac-name"]);
                    console.log(pairedColor);
                }
            });

            $(".s-hb .s-bg-l:not(.s-bg-lo)").attr("class", "s-bg-l");
            $(".s-hb .s-bg-l:not(.s-bg-lo)").attr("data-wnd_section_background", "default effects position attachment");
            $(".s-hb .s-bg-l:not(.s-bg-lo)").attr("style", '');

            $(".s-hb .s-bg-lo").attr("class", "s-bg-l s-bg-lo");

        }else if(styleData.HB_SC === "sc-cd" || styleData.HB_SC === "sc-cm" || styleData.HB_SC === "sc-cl" || styleData.HB_SC === "sc-cs" ) {

            CS_PAIRS.forEach(function(CS){
                // if(CS["cs-name"] === currentCS){
                if(pageClasses.indexOf(CS["cs-name"]) !== -1){
                    pairedColor = " " + CS["cs-pairings"][styleData.HB_SC];
                    console.log(CS["cs-name"]);
                    console.log(pairedColor);
                }
            });

            $(".s-hb .s-bg-l:not(.s-bg-lo)").attr("class", "s-bg-l");
            $(".s-hb .s-bg-l:not(.s-bg-lo)").attr("data-wnd_section_background", "default effects position attachment");
            $(".s-hb .s-bg-l:not(.s-bg-lo)").attr("style", '');

            $(".s-hb .s-bg-lo").attr("class", "s-bg-l s-bg-lo");

        }else if(styleData.HB_SC !== "sc-m" && styleData.HB_SC !== "sc-ml"){
            console.log("solid color");

            $(".s-hb .s-bg-l:not(.s-bg-lo)").attr("class", "s-bg-l");
            $(".s-hb .s-bg-l:not(.s-bg-lo)").attr("data-wnd_section_background", "default effects position attachment");
            $(".s-hb .s-bg-l:not(.s-bg-lo)").attr("style", '');

            $(".s-hb .s-bg-lo").attr("class", "s-bg-l s-bg-lo");
            // $(".s-bg-lo").attr("data-wnd_section_background", "overlays");

        }else if(styleData.HB_SC === "sc-m"){
            console.log("section media");

            $(".s-hb .s-bg-l:not(.s-bg-lo)").attr("class", "s-bg-l wnd-background-image fx-none bgpos-center-center bgatt-scroll");
            $(".s-hb .s-bg-l:not(.s-bg-lo)").attr("data-wnd_section_background", "default effects position attachment");
            $(".s-hb .s-bg-l:not(.s-bg-lo)").attr("style", 'background-image: url("https://static.d.webnodev.com/files/2w/2wp/2wp4rq.jpg?t=1");');

            $(".s-hb .s-bg-lo").attr("class", "s-bg-l s-bg-lo overlay-black");
            // $(".s-bg-lo").attr("data-wnd_section_background", "overlays");

        }else if(styleData.HB_SC === "sc-ml"){

            $(".s-hb .s-bg-l:not(.s-bg-lo)").attr("class", "s-bg-l wnd-background-image fx-none bgpos-center-center bgatt-scroll");
            $(".s-hb .s-bg-l:not(.s-bg-lo)").attr("data-wnd_section_background", "default effects position attachment");
            $(".s-hb .s-bg-l:not(.s-bg-lo)").attr("style", 'background-image: url("https://static.d.webnodev.com/files/2w/2wp/2wp4rq.jpg?t=1");');

            $(".s-hb .s-bg-lo").attr("class", "s-bg-l s-bg-lo overlay-white");
            // $(".s-bg-lo").attr("data-wnd_section_background", "overlays");

        }

        $(".s-hb").removeClass(HB_SC_CLASSES.join(" ")).addClass(styleData.HB_SC + pairedColor);

        // $("body").addClass("helpers-loaded");

    }, 200);

};

chrome.runtime.onMessage.addListener(
    function(message, sender, sendResponse) {
        switch(message.type) {
            case "applyStyles":
                applyStyles(message.styleData);
                sendResponse("Done applying styles");
                break;
        }
    }
);

$(document).ready(function(){
    chrome.storage.sync.get(['wtf-styles-data', 'wtf-auto-apply'], function (items) {
        if(typeof items['wtf-auto-apply'] !== 'undefined') {
            if(typeof items['wtf-styles-data'] !== 'undefined') {
                applyStyles(items['wtf-styles-data']);
            }
        }
    })
});
